package edu.wm.cs.cs301.slidingpuzzle;

import java.util.Objects;

public class SimplePuzzleState implements PuzzleState {

	private int[][] currentBoard;
	private int[][] newBoard; // will become next state's currentBoard
	private PuzzleState parent; // initially null by default
	private Operation operation; // initially null by default
	private int pathLen = 0;


	public SimplePuzzleState(PuzzleState priorState, Operation op, int[][] current, int pathLen) {
		this.parent = priorState;
		this.operation = op;
		this.currentBoard = current;
		this.pathLen = pathLen;
	}
	
	public SimplePuzzleState() {
	}
	
	@Override
	public void setToInitialState(int dimension, int numberOfEmptySlots) {
		currentBoard = new int[dimension][dimension];
		int tileValue = 1;
		for(int i=0; i<dimension; i++) {
			for(int j=0; j<dimension; j++) {
				if (tileValue > dimension*dimension-numberOfEmptySlots) {
					break; // If true board is full and the remaining tiles should be empty. 
				}
				currentBoard[i][j] = tileValue;
				tileValue++;
			}
		}
	}
	
	private boolean checkBounds(int row, int col) {
		int len = currentBoard.length;
		if (row >= 0 && row < len && col >= 0 && col < len) {
			return true;
		}
		return false;
	}

	@Override
	public int getValue(int row, int column) {
		return currentBoard[row][column];
	}

	@Override
	public PuzzleState getParent() {
		return parent;
	}

	@Override
	public Operation getOperation() {
		return operation;
	}

	@Override
	public int getPathLength() {
		return pathLen;
	}

	@Override
	public PuzzleState move(int row, int column, Operation op) {
		// returns a puzzle state that will result from the move. 
		// Given row/column is the STARTING position of the tile being moved
		
		newBoard = createCopy();
		if      (op == Operation.MOVERIGHT && isEmpty(row, column+1)) {
			newBoard[row][column+1] = newBoard[row][column];
			newBoard[row][column] = 0;
			PuzzleState newState = new SimplePuzzleState(this, op, newBoard, pathLen+1);
			return newState;
		}
		else if (op == Operation.MOVELEFT && isEmpty(row, column-1)) {
			newBoard[row][column-1] = newBoard[row][column];
			newBoard[row][column] = 0;
			PuzzleState newState = new SimplePuzzleState(this, op, newBoard, pathLen+1);
			return newState;
		}
		else if (op == Operation.MOVEUP && isEmpty(row-1, column)) {
			newBoard[row-1][column] = newBoard[row][column];
			newBoard[row][column] = 0;
			PuzzleState newState = new SimplePuzzleState(this, op, newBoard, pathLen+1);
			return newState;
		}
		else if (op == Operation.MOVEDOWN && isEmpty(row+1, column)) {
			newBoard[row+1][column] = newBoard[row][column];
			newBoard[row][column] = 0;
			PuzzleState newState = new SimplePuzzleState(this, op, newBoard, pathLen+1);
			return newState;
		} else {
			return null;
		}
	}
	
	private int[][] createCopy() {
		// Creates a deep copy of the current board.
		// copy is for constructing new boards that result from a Move/Drag
		int len = currentBoard.length;
		int[][] copyArray = new int[len][len];
		for(int i=0; i<len; i++)
			  for(int j=0; j<len; j++) // single line blocks don't require {} apparently. 
			    copyArray[i][j] = currentBoard[i][j];
		return copyArray;
	}
		
	@Override
	public PuzzleState drag(int startRow, int startColumn, int endRow, int endColumn) {
		PuzzleState dragResult = this;
		int rowDif = startRow - endRow;
		int colDif = startColumn - endColumn;
		
		if (rowDif == 0 && colDif == 0)
			return null; // start and end are same position
		
		// Logic below checks if adjacent space is empty and if moving to that space will get tile closer
		// to endRow or endColumn. Moves away from endRow or endColumn will not be done because
		// they aren't productive.
		
		while (rowDif != 0 || colDif != 0) {
			if (isEmpty(startRow+1, startColumn) && Math.abs(startRow+1-endRow) < Math.abs(rowDif)) {
				dragResult = dragResult.move(startRow, startColumn, Operation.MOVEDOWN); // productive to move down
				rowDif = startRow + 1 - endRow;
				startRow++;
			} else if (isEmpty(startRow-1, startColumn) && Math.abs(startRow-1-endRow) < Math.abs(rowDif)) {
				dragResult = dragResult.move(startRow, startColumn, Operation.MOVEUP);
				rowDif = startRow -1 - endRow;
				startRow--;
			} else if (isEmpty(startRow,startColumn+1) && Math.abs(startColumn+1-endColumn) < Math.abs(colDif)) {
				dragResult = dragResult.move(startRow, startColumn, Operation.MOVERIGHT);
				colDif = startColumn + 1 - endColumn;
				startColumn++;
			} else if (isEmpty(startRow,startColumn-1) && Math.abs(startColumn-1-endColumn) < Math.abs(colDif)) {
				dragResult = dragResult.move(startRow, startColumn, Operation.MOVELEFT);
				colDif = startColumn - 1 - endColumn;
				startColumn--;
			} else {
				return null;
			}
		}
		return dragResult;
	}

	@Override
	public PuzzleState shuffleBoard(int pathLength) {
		PuzzleState resultingState = this;
		int len = currentBoard.length;
		if (resultingState.getPathLength() >= pathLength)
			pathLen = 0;
		
		while (resultingState.getPathLength() < pathLength) {
			// select random tile and move it if there's an adjacent empty tile
			int row = (int)(Math.random()*len);
			int col = (int)(Math.random()*len);
			
			if (resultingState.isEmpty(row,col) && !resultingState.isEmpty(row-1,col) && checkBounds(row-1,col) && resultingState.getOperation() != Operation.MOVEUP) {
				resultingState = resultingState.move(row-1,col, Operation.MOVEDOWN);
			} else if (resultingState.isEmpty(row,col) && !resultingState.isEmpty(row,col-1) && checkBounds(row,col-1) && resultingState.getOperation() != Operation.MOVELEFT) {
				resultingState = resultingState.move(row, col-1, Operation.MOVERIGHT);
			} else if (resultingState.isEmpty(row,col) && !resultingState.isEmpty(row+1,col) && checkBounds(row+1,col) && resultingState.getOperation() != Operation.MOVEDOWN){
				resultingState = resultingState.move(row+1,col, Operation.MOVEUP);
			} else if (resultingState.isEmpty(row,col) && !resultingState.isEmpty(row,col+1) && checkBounds(row,col+1) && resultingState.getOperation() != Operation.MOVERIGHT) {
				resultingState = resultingState.move(row,col+1, Operation.MOVELEFT);
			}	
		}
		return resultingState;
	}

	@Override
	public boolean isEmpty(int row, int column) {
		if (checkBounds(row, column) && currentBoard[row][column] == 0 ) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean equals(Object state2) {
		// This method will be called to compare generic objects
		// First, the method has to check if the given object is of type simplePuzzleState
		// Once it is confirmed that state2 is a valid puzzle state, cast state2 to a SimplePuzzleState.
		// With the new validPuz, we can access the SimplePuzzleState attributes needed to test 
		// for equality. If any tiles values differ, or if the dimension of the boards differ return false
		
		if ( !(state2 instanceof SimplePuzzleState) )
			return false; // not a SimplePuzzleState, can't determine equality
		
		SimplePuzzleState validPuz = (SimplePuzzleState) state2;
		
		// These conditions below check equality when objects exist but haven't been set to initial state
		// If currentBoard is null then the initial state hasn't been set
		
		if (currentBoard == null && validPuz.currentBoard == null) {
			return true;
		} else if (currentBoard == null && validPuz.currentBoard != null) {
			return false;
		} else if (currentBoard != null && validPuz.currentBoard == null) {
			return false;
		}

		if (currentBoard.length != validPuz.currentBoard.length)
			return false;
		
		// loop through and compare each tile value
		for (int i=0; i<currentBoard.length; i++) {
			for (int j=0; j<validPuz.currentBoard.length; j++) {
				if (currentBoard[i][j] != validPuz.currentBoard[i][j]) {
					return false; // tile values don't match
				}
			}
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		// objects that evaluate to equal should have the same hash value
		int hash = 7;
		for (int i=0; i < currentBoard.length; i++) {
			for (int j=0; j < currentBoard.length; j++) {
				hash = 37 * hash + Objects.hashCode(currentBoard[i][j]);
			}
		}
		return hash;
	}
	
	@Override
	public PuzzleState getStateWithShortestPath() {
		return this;
	}
}
